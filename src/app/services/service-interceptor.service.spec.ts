import { TestBed } from '@angular/core/testing';

import { ServiceInterceptorService } from './service-interceptor.service';

describe('ServiceInterceptorService', () => {
  let service: ServiceInterceptorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceInterceptorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
