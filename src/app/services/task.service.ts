import { Task } from './../models/task.model';
import { ApirequestService } from './apirequest.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(
    private service: ApirequestService
  ) {

  }

  // *** LIST Services

  createList(title: string) {
    return this.service.post('lists', { title });
  }

  updateList(listId: string, title: string) {
    return this.service.patch('lists/' + listId, { title });
  }

  getLists() {
    return this.service.get('lists');
  }

  deleteList(id: string) {
    return this.service.delete('lists/' + id);
  }

  // *** TASK Servies

  getTasks(listId: string) {
    return this.service.get('lists/' + listId + "/tasks");
  }

  createTask(title: string, listId: string) {
    return this.service.post('lists/' + listId + "/tasks", { title });
  }

  completeTask(task: Task) {
    return this.service.patch("lists/" + task._listId + "/tasks/" + task._id, { completed: !task.completed });
  }

  deleteTask(listId: string, taskId: string) {
    return this.service.delete('lists/' + listId + '/tasks/' +  taskId);
  }


  updateTask(listId: string, taskId: string, title: string) {
    return this.service.patch('lists/' + listId + '/tasks/' +  taskId, { title });
  }
}
