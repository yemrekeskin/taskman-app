import { List } from './../../models/list.model';
import { Task } from './../../models/task.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { TaskService } from 'src/app/services/task.service';

@Component({
  selector: 'app-task-view',
  templateUrl: './task-view.component.html',
  styleUrls: ['./task-view.component.scss']
})
export class TaskViewComponent implements OnInit {

  lists: List[];
  tasks: Task[];

  selectedListId: string;

  constructor(
    private taskService: TaskService,
    private route : ActivatedRoute,
    private router: Router
  ) {

  }

  ngOnInit(): void
  {
    this.route.params.subscribe(
      (params: Params) => {
        console.log(params);

        if(params.listId) {
          this.taskService.getTasks(params.listId).subscribe((tasks: Task[]) => {
            console.log(tasks);
            this.selectedListId = params.listId;
            this.tasks = tasks;
          });
        } else {
          this.tasks = undefined;
        }

      });


    this.taskService.getLists().subscribe((lists: List[]) => {
      this.lists = lists;
      console.log(lists);
    })
  }

  onTaskClick(task: Task) {
    task._listId = this.selectedListId;
    this.taskService.completeTask(task).subscribe(() => {
      console.log('Completed successfully');
      task.completed =  !task.completed;
    });
  }

  onDeleteListClick() {
    this.taskService.deleteList(this.selectedListId).subscribe((res: any)=> {
      console.log(res);
      this.router.navigate(['/lists']);
    })
  }

  onDeleteTaskClick(id: string) {
    this.taskService.deleteTask(this.selectedListId, id).subscribe((res: any)=> {
      this.tasks = this.tasks.filter(d=> d._id !== id);
      console.log(res);
    })
  }
}
